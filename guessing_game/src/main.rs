use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Let's play Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    println!("Guess a number...");

    loop{
        let mut user_guess = String::new();

        io::stdin()
            .read_line(&mut user_guess)
            .expect("Failed to read line");

        let user_guess: u32 = match user_guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        println!("You guessed: {user_guess}");

        match user_guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small! Try again..."),
            Ordering::Greater => println!("Too big! Try again..."),
            Ordering::Equal => {
                println!("You win");
                break;
            }
        }
    }
}
